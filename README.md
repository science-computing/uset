Runing Application
==================

Developer (docker)
------------------

```
sudo apt-get install docker.io
# Arch: sudo pacman -Syu docker && sudo systemctl start docker
sudo docker build -t app .
#sudo docker build .
#docker images
sudo docker run -it app
```

Developer (local system)
------------------------

```
apt-get install python3 python3-pip libsqlite3-dev libpq-dev
pip3 install --upgrade pip
pip3 install -r requirements.txt
python3 manage.py migrate
python3 manage.py createsuperuser admin admin
python3 manage.py runserver
```


Production
------------

Use 3 server roles
 - frontend (nginx)
 - application server (django)
 - database server (postgresql)

each role should be handled by a different server.
