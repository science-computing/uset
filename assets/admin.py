from django.contrib import admin
from .models import Department, AssetSchema, Asset


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    model = Department
    list_display = ('name', 'short', 'num_schemas')

    def num_schemas(self, obj):
        return obj.default_schemas.all().count()


@admin.register(AssetSchema)
class AssetSchemaAdmin(admin.ModelAdmin):
    model = AssetSchema
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Asset)
class AssetAdmin(admin.ModelAdmin):
    model = Asset



