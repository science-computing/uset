from django.conf.urls import url, include
from django.contrib.auth.models import User
from assets.models import Asset, AssetSchema
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
class AssetSchemaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AssetSchema
        fields = ('name', 'slug', 'schema')


class AssetSerializer(serializers.HyperlinkedModelSerializer):
    schema = serializers.SlugRelatedField(
                   read_only=True,
                   slug_field='slug'
                   )
    class Meta:
        model = Asset
        fields = ('uid', 'date_created', 'date_edited', 'schema', 'data')


# ViewSets define the view behavior.
class AssetSchemaViewSet(viewsets.ModelViewSet):
    queryset = AssetSchema.objects.all()
    serializer_class = AssetSchemaSerializer

class AssetViewSet(viewsets.ModelViewSet):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'assets', AssetViewSet)
router.register(r'asset_schemas', AssetSchemaViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
