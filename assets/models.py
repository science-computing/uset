from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField


class AssetSchema(models.Model):
    name = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256, unique=True)
    schema_raw = models.TextField()
    schema = JSONField(default=dict)

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=256, unique=True)
    short = models.CharField(max_length=64, unique=True)
    default_schemas = models.ManyToManyField(AssetSchema, blank=True)

    def save(self, *args, **kwargs):
        self.short = self.short.upper()
        super(Department, self).save(*args, *kwargs)

    def __str__(self):
        return "{0} [{1}]".format(self.name, self.short)


class Asset(models.Model):
    uid = models.CharField(max_length=256, primary_key=True)

    date_created = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    department = models.ForeignKey(Department)
    schema = models.ForeignKey(AssetSchema, blank=True)

    data = JSONField(default=dict)


class Attachment(models.Model):
    file = models.FileField(upload_to="attachments")
    date_created = models.DateTimeField(auto_now_add=True)
    asset = models.ForeignKey(Asset)
