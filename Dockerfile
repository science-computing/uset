FROM ubuntu:16.04
##MAINTAINER Steve Weber "<s8weber@uwaterloo.ca>"
EXPOSE 8000

#WORKDIR /app
#VOLUME ["/data"]
RUN sed -i 's|http://mirrors.us.kernel.org|http://mirror.csclub.uwaterloo.ca|g' /etc/apt/sources.list
RUN sed -i 's|http://ca.archive.ubuntu.com|http://mirror.csclub.uwaterloo.ca|g' /etc/apt/sources.list
RUN sed -i 's|http://security.ubuntu.com|http://mirror.csclub.uwaterloo.ca|g' /etc/apt/sources.list
RUN sed -i 's|http://archive.ubuntu.com|http://mirror.csclub.uwaterloo.ca|g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y python3 python3-pip libsqlite3-dev libpq-dev

RUN mkdir /srv/app
COPY ./ /srv/app/
WORKDIR /srv/app

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN python3 manage.py migrate
RUN echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')" | python3 manage.py shell

ENTRYPOINT exec python3 manage.py runserver
